types = csc ece phy gen
sectionNames = title1 par1 title2 par2
sections = $(sectionNames:=.tex)
outputs = $(types:=.pdf)

all: $(outputs)

template.pdf: template.tex
	echo $^;
	mkdir -p /tmp/lug-ads
	$(foreach section, $(sections), touch /tmp/lug-ads/$(section);)
	latexmk -pdf template.tex
	rm -r /tmp/lug-ads

%.pdf: %/ template.tex
	mkdir -p /tmp/lug-ads
	$(foreach section, $(sections), cp $(@:.pdf=/$(section)) /tmp/lug-ads/$(section);)
	latexmk -pdf -silent -jobname=$(@:.pdf=) template.tex
	rm -r /tmp/lug-ads

.PHONY: clean
clean:
	$(foreach type, $(types), latexmk -c -silent -jobname=$(type) template.tex;)
	latexmk -c -silent template.tex
	if [[ -d /tmp/lug-ads ]]; then rm -r /tmp/lug-ads; fi
