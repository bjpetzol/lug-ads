Posters for the LUG @ NCSU.
Expects GNU make, latexmk, and standard Linux utilities/environment.
The folders contain text for different departments, injected into the general template.
"Make clean" is provided, always run it before committing.

csc: Computer Science  
ece: Electrical and Computer Engineering  
phy: Physics  
gen: General
